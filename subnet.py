import ipaddress

class Subnet:
    def __init__(self, ip_address, subnet_mask):
        self.ip_address = ip_address
        self.subnet_mask = subnet_mask
        self.subnet_bits = 0
        self.max_subnets = 0
        self.hosts_per_subnet = 0
        self.network_address = 0
        self.broadcast_address = 0
        self.first_host_address = 0
        self.last_host_address = 0
        self.subnet_id = ""
        self.hex_ip_address = ""
        self.wildcard_mask = ""
        self.subnet_bitmap = ""
        self.mask_bits = ""
        self.first_octet_range = ""
        self.prefix = ""
        self.calculate_subnet_details()

    def is_valid_ip_address(self, ip_address):
        try:
            ipaddress.IPv4Network(ip_address, strict=False)
            return True
        except ValueError:
            return False

    def is_valid_subnet_mask(self, subnet_mask):
        try:
            ipaddress.IPv4Network(f"0.0.0.0/{subnet_mask}", strict=False)
            return True
        except ValueError:
            return False

    def calculate_subnet_details(self):
        if self.is_valid_ip_address(self.ip_address) and self.is_valid_subnet_mask(self.subnet_mask):
            ip = int(ipaddress.IPv4Address(self.ip_address))
            mask = int(ipaddress.IPv4Address(self.subnet_mask))
            self.subnet_bits = bin(mask).count('1')
            self.max_subnets = 2**(32 - self.subnet_bits)
            self.hosts_per_subnet = self.max_subnets - 2
            step = 2**(32 - self.subnet_bits)

            self.network_address = ip & mask
            self.broadcast_address = self.network_address + step - 1

            self.first_host_address = self.network_address + 1
            self.last_host_address = self.broadcast_address - 1

            self.subnet_id = ipaddress.IPv4Address(self.network_address)
            self.hex_ip_address = hex(ip)
            self.wildcard_mask = ipaddress.IPv4Address(0xFFFFFFFF - mask)

            subnet_bits_str = bin(mask)[2:]
            self.subnet_bitmap = ".".join(['n' if bit == '1' else 'h' for bit in subnet_bits_str.zfill(32)])
            self.mask_bits = str(self.subnet_bits)
            
            first_octet = (ip & mask) >> 24
            if 1 <= first_octet <= 126:
                self.prefix = "0"
                self.first_octet_range = "1 - 126"
            elif 128 <= first_octet <= 191:
                self.prefix = "10"
                self.first_octet_range = "128 - 191"
            elif 192 <= first_octet <= 223:
                self.prefix = "110"
                self.first_octet_range = "192 - 223"
            elif 224 <= first_octet <= 239:
                self.first_octet_range = "Multicast"
            elif 240 <= first_octet <= 255:
                self.first_octet_range = "Reserved"
            else:
                self.first_octet_range = "Not in the defined IP address classes"

    def display_subnet_details(self):
        print("IP Address:", self.ip_address)
        print("Hex IP Address:", self.hex_ip_address)
        print("Subnet Mask:", self.subnet_mask)
        print("Subnet Bits:", self.subnet_bits)
        print("Maximum Subnets:", self.max_subnets)
        print("Hosts per Subnet:", self.hosts_per_subnet)
        print("Subnet ID:", self.subnet_id)
        print("Host Address Range:", str(ipaddress.IPv4Address(self.first_host_address)) + " - " + str(ipaddress.IPv4Address(self.last_host_address)))
        print("Broadcast Address:", str(ipaddress.IPv4Address(self.broadcast_address)))
        print("Wildcard Mask:", self.wildcard_mask)
        print("Subnet Bitmap:", self.subnet_bitmap)
        print("Mask Bits:", self.mask_bits)
        print("First Octet Range:", self.first_octet_range)

if __name__ == "__main__":
    ip_address = input("Enter IP address: ")
    subnet_mask = input("Enter subnet mask: ")

    network = Subnet(ip_address, subnet_mask)
    network.display_subnet_details()
